mod authproto;

use xringlock::error::GenericError;
use xringlock::implementation::backend::xsecurelock::XSecurelockBackend;
use xringlock::implementation::indicator::ring::RingIndicator;

use crate::authproto::{AuthprotoConnection, Request, Response};
use secrets::Secret;
use std::ffi::OsStr;
use std::time::Instant;
use xringlock::backend::Backend;
use xringlock::indicator::{RenderOptions, RenderReason};

/// Maximal length for entered passwords.
///
/// Advantages:
/// - Secret can be stored on the stack.
/// - Malicious users cannot cause buffer overflows or out-of-memory conditions by typing lot's of chars.
const MAX_PASSWORD_LENGTH: usize = 256;

fn main() -> Result<(), GenericError> {
    let mut back = XSecurelockBackend::from_env_or_exec()?;
    let mut ind = RingIndicator::new();
    let mut opts = RenderOptions::default();
    let mut authproto = AuthprotoConnection::new(
        AsRef::<OsStr>::as_ref("/usr/lib/xsecurelock/authproto_pam").into(),
    );
    let epoch = Instant::now();

    back.draw(epoch.elapsed(), &mut ind, RenderReason::Init, &mut opts)?;

    Secret::<[u8; MAX_PASSWORD_LENGTH]>::zero(|mut pw| {
        Secret::<[u8; MAX_PASSWORD_LENGTH]>::zero(|mut resp| loop {
            let (req, resp_len) = Request::receive(&mut authproto, &mut resp)?;
            match req {
                Request::Error | Request::Info => println!("PAM: {}", str(&resp[..resp_len])),
                Request::PromptLikeUsername => todo!("username queries"),
                Request::PromptLikePassword => {
                    let len = xringlock::prompt(&mut pw, epoch, &mut ind, &mut back, &mut opts)?;
                    Response::LikePassword.reply(&mut authproto, &pw, len)?;
                }
                Request::Success => return Ok(()),
            }
        })
    })
}

fn str(s: &[u8]) -> String {
    String::from_utf8(Vec::from(s)).unwrap()
}
