use crate::authproto::AuthprotoError::UnknownRequestType;
use err_derive::Error;
use std::borrow::Cow;
use std::ffi::OsStr;
use std::io;
use std::io::{ErrorKind, Read, Write};
use std::ops::DerefMut;
use std::process::{Child, Command, Stdio};
use std::string::FromUtf8Error;
use xringlock::error::{BackendError, GenericError};
use xringlock::util::ReadCharExt;

const AUTH_SUCCESS: u8 = b'\x06';

#[derive(Error, Debug)]
pub enum AuthprotoError {
    #[error(display = "tried to read from child eventhough it was not running")]
    ChildNotRunning,

    #[error(display = "received malformed message from child")]
    MalformedMessage,

    #[error(display = "received unknown request type: {:?}", _0)]
    UnknownRequestType(char),

    #[error(display = "invalid message length: {}", _0)]
    InvalidMessageLength(usize),

    #[error(display = "child sent malformed UTF-8")]
    UTF8Error(#[source] FromUtf8Error),

    #[error(display = "io error: {}", _0)]
    IoError(#[source] io::Error),
}

impl BackendError for AuthprotoError {
    fn backend_name(&self) -> Cow<'static, str> {
        "xsecurelock".into()
    }
}

impl From<AuthprotoError> for GenericError {
    fn from(e: AuthprotoError) -> Self {
        Self::BackendError(Box::new(e))
    }
}

pub enum Request {
    Info,
    Error,
    PromptLikeUsername,
    PromptLikePassword,
    Success,
}

impl Request {
    pub fn receive<R: DerefMut<Target = [u8; CAP]>, const CAP: usize>(
        conn: &mut AuthprotoConnection,
        resp: &mut R,
    ) -> Result<(Request, usize), AuthprotoError> {
        let (typ, len) = conn.receive(resp)?;
        Ok((
            match typ {
                b'i' => Request::Info,
                b'e' => Request::Error,
                b'U' => Request::PromptLikeUsername,
                b'P' => Request::PromptLikePassword,
                AUTH_SUCCESS => Request::Success,
                _ => return Err(UnknownRequestType(typ as char)),
            },
            len,
        ))
    }
}

#[allow(dead_code)]
pub enum Response {
    LikeUsername,
    LikePassword,
    Cancelled,
}

impl Response {
    pub fn reply<M: DerefMut<Target = [u8; CAP]>, const CAP: usize>(
        &self,
        conn: &mut AuthprotoConnection,
        msg: &M,
        len: usize,
    ) -> Result<(), AuthprotoError> {
        let typ = match self {
            Response::LikeUsername => b'u',
            Response::LikePassword => b'p',
            Response::Cancelled => b'x',
        };
        conn.send(typ, msg, len)
    }
}

pub struct AuthprotoConnection {
    executable: Cow<'static, OsStr>,
    process: Option<Child>,
}

impl AuthprotoConnection {
    pub fn new(exe: Cow<'static, OsStr>) -> Self {
        Self {
            executable: exe,
            process: None,
        }
    }

    fn send<M: DerefMut<Target = [u8; CAP]>, const CAP: usize>(
        &mut self,
        typ: u8,
        msg: &M,
        len: usize,
    ) -> Result<(), AuthprotoError> {
        if let Some(child) = &mut self.process {
            if let Some(tx) = &mut child.stdin {
                tx.write_all(format!("{} {}\n", typ as char, len).as_bytes())?;
                tx.write_all(&msg[..len])?;
                tx.write_all(&[b'\n'])?;
                Ok(())
            } else {
                unreachable!()
            }
        } else {
            panic!("child not launched");
        }
    }

    fn receive<R: DerefMut<Target = [u8; CAP]>, const CAP: usize>(
        &mut self,
        resp: &mut R,
    ) -> Result<(u8, usize), AuthprotoError> {
        if self.process.is_none() {
            self.process = Some(
                Command::new(&self.executable)
                    .stdin(Stdio::piped())
                    .stdout(Stdio::piped())
                    .stderr(Stdio::inherit())
                    .spawn()?,
            );
        }

        if let Some(child) = &mut self.process {
            if let Some(rx) = &mut child.stdout {
                let typ = match rx.read_byte() {
                    Ok(typ) => typ,
                    Err(e) => {
                        if e.kind() == ErrorKind::UnexpectedEof && child.wait()?.success() {
                            return Ok((AUTH_SUCCESS, 0));
                        }
                        return Err(e.into());
                    }
                };
                if rx.read_byte()? != b' ' {
                    return Err(AuthprotoError::MalformedMessage);
                }
                let mut l = 0;
                loop {
                    let digit = rx.read_byte()?;
                    if digit == b'\n' {
                        break;
                    } else if (b'0'..=b'9').contains(&digit) {
                        l = l * 10 + (digit - b'0') as usize
                    } else {
                        return Err(AuthprotoError::MalformedMessage);
                    }
                }

                if l > CAP {
                    return Err(AuthprotoError::InvalidMessageLength(l));
                }

                rx.read_exact(&mut resp.deref_mut()[..l])?;
                if rx.read_byte()? != b'\n' {
                    return Err(AuthprotoError::MalformedMessage);
                }

                return Ok((typ, l));
            }
        }
        unreachable!()
    }
}

impl Drop for AuthprotoConnection {
    fn drop(&mut self) {
        if let Some(child) = &mut self.process {
            child.kill().ok();
        }
    }
}
