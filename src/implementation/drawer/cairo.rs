use crate::drawer::{Drawer, Point, RGBA};
use crate::error::DrawerError;
use cairo::{Context, Operator, XCBSurface};
use err_derive::Error;
use std::borrow::Cow;

#[derive(Error, Debug)]
pub enum CairoError {}

impl DrawerError for CairoError {
    fn drawer_name(&self) -> Cow<'static, str> {
        "cairo".into()
    }
}

pub struct CairoDrawer {
    cr: Context,
    surface: XCBSurface,
    width: u16,
    height: u16,
}

impl CairoDrawer {
    pub fn from_surface(surface: XCBSurface, width: u16, height: u16) -> CairoDrawer {
        CairoDrawer {
            cr: Context::new(&surface),
            surface,
            width,
            height,
        }
    }
}

impl Drawer for CairoDrawer {
    fn id(&self) -> u64 {
        self.surface.to_raw_none() as u64
    }

    fn dimensions(&self) -> (f64, f64) {
        (self.width as f64, self.height as f64)
    }

    fn use_color(&mut self, c: RGBA) {
        self.cr.set_source_rgba(c.0, c.1, c.2, c.3);
    }

    fn use_stroke_width(&mut self, w: f64) {
        self.cr.set_line_width(w);
    }

    fn arc(&mut self, center: Point, radius: f64, start_angle: f64, end_angle: f64) {
        self.cr
            .arc(center.0, center.1, radius, start_angle, end_angle);
    }

    fn line(&mut self, a: Point, b: Point) {
        self.cr.move_to(a.0, a.1);
        self.cr.line_to(b.0, b.1);
    }

    fn fill_background(&mut self) {
        self.cr.set_operator(Operator::Source);
        self.cr.paint();
    }

    fn fill(&mut self) {
        self.cr.fill_preserve();
    }

    fn stroke(&mut self) {
        self.cr.stroke_preserve();
    }

    fn flush(&mut self) {
        self.surface.flush();
    }
}
