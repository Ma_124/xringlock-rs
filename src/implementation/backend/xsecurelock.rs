use crate::backend::{Backend, BackendEvent};
use crate::drawer::Drawer;
use crate::error::{BackendError, GenericError};
use crate::implementation::drawer::cairo::CairoDrawer;
use crate::indicator::{Indicator, RenderOptions, RenderReason};
use crate::util::ReadCharExt;
use cairo::XCBSurface;
use err_derive::Error;
use std::borrow::Cow;
use std::cell::RefCell;
use std::env::VarError;
use std::ffi::OsString;
use std::io::stdin;
use std::os::unix::process::CommandExt;
use std::process::Command;
use std::ptr::NonNull;
use std::time::Duration;
use std::{env, io};
use x11rb::connection::Connection;
use x11rb::errors::{ConnectError, ConnectionError, ReplyError, ReplyOrIdError};
use x11rb::protocol::randr::ConnectionExt;
use x11rb::protocol::xproto::{
    AtomEnum, ConfigureWindowAux, ConnectionExt as XProtoConnectionExt, CreateWindowAux, EventMask,
    PropMode, StackMode, Window, WindowClass,
};
use x11rb::protocol::Event;
use x11rb::wrapper::ConnectionExt as WrapperConnectionExt;
use x11rb::x11_utils::{Serialize, X11Error};
use x11rb::xcb_ffi::XCBConnection;
use x11rb::COPY_DEPTH_FROM_PARENT;

#[derive(Error, Debug)]
pub enum XSecurelockError {
    #[error(display = "could not connect to X server: {}", _0)]
    XConnectError(#[source] ConnectError),

    #[error(display = "connection to X server failed: {}", _0)]
    XConnectionError(#[source] ConnectionError),

    #[error(display = "x error: {:?}", _0)]
    X11Error(#[error(from)] X11Error),

    #[error(display = "could not find a matching visual")]
    XNoVisual,

    #[error(display = "all available IDs for X have been exhausted")]
    XIdsExhausted,

    #[error(display = "cairo error: {}", _0)]
    CairoError(#[source] cairo::Error),

    #[error(display = "io error: {}", _0)]
    IoError(#[source] io::Error),

    #[error(display = "failed parsing XSCREENSAVER_WINDOW={:?}", _0)]
    InvalidScreenlockWindow(OsString),

    #[error(display = "end of keyboard stream provided by xsecurelock")]
    EndOfKeyStream,

    #[error(display = "failed to find path to this executable")]
    WhereAmI,
}

impl From<ReplyOrIdError> for XSecurelockError {
    fn from(e: ReplyOrIdError) -> Self {
        match e {
            ReplyOrIdError::IdsExhausted => XSecurelockError::XIdsExhausted,
            ReplyOrIdError::ConnectionError(e) => XSecurelockError::XConnectionError(e),
            ReplyOrIdError::X11Error(e) => XSecurelockError::X11Error(e),
        }
    }
}

impl From<ReplyError> for XSecurelockError {
    fn from(e: ReplyError) -> Self {
        match e {
            ReplyError::ConnectionError(e) => XSecurelockError::XConnectionError(e),
            ReplyError::X11Error(e) => XSecurelockError::X11Error(e),
        }
    }
}

impl BackendError for XSecurelockError {
    fn backend_name(&self) -> Cow<'static, str> {
        "xsecurelock".into()
    }
}

impl From<XSecurelockError> for GenericError {
    fn from(e: XSecurelockError) -> Self {
        GenericError::BackendError(Box::new(e))
    }
}

pub struct XSecurelockBackend {
    dpy: XCBConnection,
    drawers: Vec<RefCell<Box<dyn Drawer>>>,
}

impl XSecurelockBackend {
    pub fn new(screensaver_window: Window) -> Result<XSecurelockBackend, GenericError> {
        Self::create(screensaver_window).map_err(|e| GenericError::BackendError(Box::new(e)))
    }

    pub fn from_env_or_exec() -> Result<XSecurelockBackend, GenericError> {
        let wid_res = env::var("XSCREENSAVER_WINDOW");
        if matches!(wid_res, Err(VarError::NotPresent)) {
            if let Some(path) = whereami::executable_path() {
                let mut args = env::args();
                args.next(); // skip argv[0]
                Command::new("xsecurelock")
                    .args(args)
                    .env("XSECURELOCK_AUTH", path)
                    .exec();
                panic!("exec failed")
            } else {
                return Err(GenericError::from(XSecurelockError::WhereAmI));
            }
        }
        match wid_res {
            Err(_) => Err(GenericError::from(
                XSecurelockError::InvalidScreenlockWindow(
                    env::var_os("XSCREENSAVER_WINDOW").unwrap_or_else(|| "".into()),
                ),
            )),
            Ok(wid) => {
                if let Ok(wid) = wid.parse::<u32>() {
                    Ok(Self::create(wid)?)
                } else {
                    Err(GenericError::from(
                        XSecurelockError::InvalidScreenlockWindow(wid.into()),
                    ))
                }
            }
        }
    }

    fn create(mut screensaver_window: Window) -> Result<XSecurelockBackend, XSecurelockError> {
        let (dpy, screen_id) = XCBConnection::connect(None)?;
        let screen = &dpy.setup().roots[screen_id];

        if screensaver_window == 0 {
            screensaver_window = screen.root
        }

        let tree = dpy.query_tree(screensaver_window)?.reply()?;
        let monitors = dpy.randr_get_monitors(tree.root, true)?.reply()?;
        let mut drawers: Vec<RefCell<Box<dyn Drawer>>> =
            Vec::with_capacity(monitors.monitors.len());

        let mut cfg = ConfigureWindowAux::new();
        cfg.sibling = Some(screensaver_window);
        cfg.stack_mode = Some(StackMode::BELOW);

        for monitor in monitors.monitors {
            let (w, h) = (monitor.width, monitor.height);

            let wid = dpy.generate_id()?;
            dpy.create_window(
                COPY_DEPTH_FROM_PARENT,
                wid,
                tree.parent,
                monitor.x,
                monitor.y,
                w,
                h,
                0,
                WindowClass::INPUT_OUTPUT,
                screen.root_visual,
                &CreateWindowAux::default().event_mask(EventMask::EXPOSURE | EventMask::KEY_PRESS),
            )?;
            dpy.change_property8(
                PropMode::REPLACE,
                wid,
                AtomEnum::WM_NAME,
                AtomEnum::STRING,
                "xringlock".as_bytes(),
            )?;
            dpy.configure_window(wid, &cfg)?;
            dpy.map_window(wid)?;

            let mut visualtype = None;
            for depth in &screen.allowed_depths {
                for vt in &depth.visuals {
                    if vt.visual_id == screen.root_visual {
                        visualtype = Some(vt);
                    }
                }
            }

            let visualtype = visualtype.ok_or(XSecurelockError::XNoVisual)?.serialize();
            let cairo_dpy;
            let cairo_visualtype;

            unsafe {
                cairo_dpy = cairo::XCBConnection::from_raw_none(
                    dpy.get_raw_xcb_connection() as *mut cairo_sys::xcb_connection_t
                );
                cairo_visualtype = cairo::XCBVisualType(NonNull::new_unchecked(
                    visualtype.as_ptr() as *mut cairo_sys::xcb_visualtype_t,
                ));
            }

            let surface = XCBSurface::create(
                &cairo_dpy,
                &cairo::XCBDrawable(wid),
                &cairo_visualtype,
                w as i32,
                h as i32,
            )?;

            drawers.push(RefCell::new(Box::new(CairoDrawer::from_surface(
                surface, w, h,
            ))));
        }
        Ok(XSecurelockBackend { dpy, drawers })
    }

    fn backend_event(&mut self, ev: Event) -> Result<Option<BackendEvent>, XSecurelockError> {
        match ev {
            Event::Error(e) => Err(XSecurelockError::from(e)),

            // The amount of `Expose` events following this one. Simple applications that do
            // not want to optimize redisplay by distinguishing between subareas of its window
            // can just ignore all Expose events with nonzero counts and perform full
            // redisplays on events with zero counts.
            //
            // [xcb_expose_event_t(3)](https://www.x.org/releases/current/doc/man/man3/xcb_expose_event_t.3.xhtml)
            Event::Expose(ev) => {
                if ev.count == 0 {
                    Ok(Some(BackendEvent::Render))
                } else {
                    Ok(None)
                }
            }
            Event::ResizeRequest(_) | Event::RandrScreenChangeNotify(_) => {
                eprintln!("screen changes not yet implemented");
                Ok(None)
            }
            _ => Ok(None),
        }
    }

    fn char_to_event(c: char) -> Result<Option<BackendEvent>, XSecurelockError> {
        match c {
            // BS | DEL
            '\x08' | '\x7F' => Ok(Some(BackendEvent::Backspace)),
            // ESC | C-a | C-u
            '\x1B' | '\x01' | '\x15' => Ok(Some(BackendEvent::Clear)),
            // CR | LF
            '\r' | '\n' => Ok(Some(BackendEvent::Enter)),
            // ASCII control chars
            '\0'..='\x1F' => Ok(None),
            // other graphic chars and space
            _ => Ok(Some(BackendEvent::Char(c))),
        }
    }

    fn poll_x_event(&mut self) -> Result<Option<BackendEvent>, GenericError> {
        loop {
            if let Some(ev) = self.dpy.poll_for_event().map_err(XSecurelockError::from)? {
                if let Some(ev) = self.backend_event(ev)? {
                    return Ok(Some(ev));
                }
            } else {
                return Ok(None);
            }
        }
    }
}

impl Backend for XSecurelockBackend {
    fn flush(&mut self) -> Result<(), GenericError> {
        Ok(self
            .dpy
            .flush()
            .map_err(XSecurelockError::XConnectionError)?)
    }

    fn draw(
        &mut self,
        time: Duration,
        indicator: &mut dyn Indicator,
        reason: RenderReason,
        opts: &mut RenderOptions,
    ) -> Result<(), GenericError> {
        indicator.render(time, &mut self.drawers[..], reason, opts)?;
        Ok(())
    }

    fn next_event(&mut self) -> Result<BackendEvent, GenericError> {
        loop {
            if let Some(ev) = self.poll_x_event()? {
                return Ok(ev);
            }
            let k = stdin().read_char().map_err(XSecurelockError::IoError)?;
            if let Some(ev) = Self::char_to_event(k)? {
                return Ok(ev);
            }
        }
    }

    fn poll_event(&mut self, timeout_millis: u16) -> Result<Option<BackendEvent>, GenericError> {
        if let Some(ev) = self.poll_x_event()? {
            return Ok(Some(ev));
        }
        let mut fds = libc::pollfd {
            fd: 0, // STDIN
            events: libc::POLLIN | libc::POLLRDBAND | libc::POLLPRI,
            revents: 0,
        };
        unsafe {
            if libc::poll(&mut fds as *mut libc::pollfd, 1, timeout_millis as i32) < 0 {
                return Err(XSecurelockError::from(io::Error::last_os_error()).into());
            }
        }
        if fds.revents != 0 {
            let k = stdin().read_char().map_err(XSecurelockError::IoError)?;
            if let Some(ev) = Self::char_to_event(k)? {
                return Ok(Some(ev));
            }
        }
        Ok(None)
    }
}
