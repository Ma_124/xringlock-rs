use crate::drawer::{Color, Drawer, Point, RGBA};
use crate::error::GenericError;
use crate::indicator::{Indicator, RenderOptions, RenderReason};
use easings_rs::{ease, Easing};
use err_derive::Error;
use std::cell::RefCell;
use std::convert::TryInto;
use std::f64::consts;
use std::time::Duration;

#[derive(Error, Debug)]
pub enum RingError {}

#[allow(dead_code)]
pub struct RingConfig {
    /// specifies the fill color of the ring.
    ring_fill_color: Color,

    /// specifies the fill color of the ring after a keypress.
    ring_fill_color_key: Color,

    /// specifies the fill color of the ring after a backspace keypress.
    ring_fill_color_back: Color,

    /// specifies the fill color of the ring after a clear keypress.
    ring_fill_color_clear: Color,

    /// specifies the fill color of the ring during authentication.
    ring_fill_color_auth: Color,

    /// specifies the fill color of the ring if caps lock is enabled.
    ring_fill_color_caps: Color,

    /// specifies the border color of the ring.
    ring_border_color: Color,

    /// specifies the border color of the ring after a keypress.
    ring_border_color_key: Color,

    /// specifies the border color of the ring after a backspace keypress.
    ring_border_color_back: Color,

    /// specifies the border color of the ring after a clear keypress.
    ring_border_color_clear: Color,

    /// specifies the border color of the ring during authentication.
    ring_border_color_auth: Color,

    /// specifies the border color of the ring if caps lock is enabled.
    ring_border_color_caps: Color,
}

pub struct RingIndicator {
    cfg: RingConfig,
    caps: bool,
    last_colors: (Color, Color),
    last_time: Duration,
}

impl RingIndicator {
    pub fn new() -> RingIndicator {
        RingIndicator {
            cfg: RingConfig {
                ring_fill_color: "#333333cc".try_into().unwrap(),
                ring_fill_color_key: "none".try_into().unwrap(),
                ring_fill_color_back: "none".try_into().unwrap(),
                ring_fill_color_clear: "none".try_into().unwrap(),
                ring_fill_color_auth: "none".try_into().unwrap(),
                ring_fill_color_caps: "#f44336".try_into().unwrap(),
                ring_border_color: "none".try_into().unwrap(),
                ring_border_color_key: "#8bc34a".try_into().unwrap(),
                ring_border_color_back: "#ff9800".try_into().unwrap(),
                ring_border_color_clear: "#f44336".try_into().unwrap(),
                ring_border_color_auth: "#3f51b5".try_into().unwrap(),
                ring_border_color_caps: "none".try_into().unwrap(),
            },
            caps: false,
            last_colors: (Color::none(), Color::none()),
            last_time: Duration::from_secs(0),
        }
    }

    fn ease(&self, now: Duration, curve: Easing, dur: Duration) -> f64 {
        ease(
            curve,
            1.0 - ((now - self.last_time).as_secs_f32() / dur.as_secs_f32()).clamp(0.0, 1.0),
        ) as f64
    }
}

impl Indicator for RingIndicator {
    #[allow(clippy::many_single_char_names)]
    fn render(
        &mut self,
        time: Duration,
        drawers: &mut [RefCell<Box<dyn Drawer>>],
        reason: RenderReason,
        opts: &mut RenderOptions,
    ) -> Result<(), GenericError> {
        let ease_dur = Duration::from_millis(250); // TODO

        let mut colors_changed = true;
        let colors = match reason {
            RenderReason::Init => (Color::none(), Color::none()),
            RenderReason::CharHash(_) | RenderReason::CharData(_) => (
                self.cfg.ring_border_color_key.clone(),
                self.cfg.ring_fill_color_key.clone(),
            ),
            RenderReason::Backspace => (
                self.cfg.ring_border_color_back.clone(),
                self.cfg.ring_fill_color_back.clone(),
            ),
            RenderReason::Clear => (
                self.cfg.ring_border_color_clear.clone(),
                self.cfg.ring_fill_color_clear.clone(),
            ),
            RenderReason::AuthStart => (
                self.cfg.ring_border_color_auth.clone(),
                self.cfg.ring_fill_color_auth.clone(),
            ),
            RenderReason::CapslockChanged(c) => {
                self.caps = c;
                self.last_colors.clone()
            }
            _ => {
                colors_changed = false;
                self.last_colors.clone()
            }
        };

        if colors_changed {
            self.last_time = time;
            opts.frames_per_second = 60;
        }

        let a = self.ease(time, Easing::SineOut, ease_dur);
        if a == 0.0 {
            opts.frames_per_second = 0
        }

        for d in drawers {
            let d = d.get_mut();
            let (w, h) = d.dimensions();
            let m = Point(w / 2.0, h / 2.0);

            d.use_color(RGBA(0.5, 0.5, 0.5, 1.0));
            d.fill_background();

            d.arc(m, 100.0, 0.0, consts::TAU);
            if self.caps {
                d.use_color(self.cfg.ring_fill_color_caps.rgba());
                d.fill();
            }
            d.use_color(colors.1.rgba().with_alpha(a));
            d.fill();

            d.arc(m, 105.0, 0.0, consts::TAU);
            d.use_stroke_width(10.0);
            if self.caps {
                d.use_color(self.cfg.ring_border_color_caps.rgba());
                d.stroke();
            }
            d.use_color(colors.0.rgba().with_alpha(a));
            d.stroke();

            d.flush();
        }

        self.last_colors = colors;

        Ok(())
    }
}
