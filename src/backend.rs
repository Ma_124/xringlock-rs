use crate::error::GenericError;
use crate::indicator::{Indicator, RenderOptions, RenderReason};
use std::time::Duration;

#[non_exhaustive]
#[derive(Debug)]
pub enum BackendEvent {
    Char(char),
    Backspace,
    Clear,
    Enter,
    Render,
}

pub trait Backend {
    fn flush(&mut self) -> Result<(), GenericError>;
    fn draw(
        &mut self,
        time: Duration,
        indicator: &mut dyn Indicator,
        reason: RenderReason,
        opts: &mut RenderOptions,
    ) -> Result<(), GenericError>;
    fn next_event(&mut self) -> Result<BackendEvent, GenericError>;
    fn poll_event(&mut self, timeout_millis: u16) -> Result<Option<BackendEvent>, GenericError>;
}
