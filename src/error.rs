use err_derive::Error;
use std::borrow::Cow;
use std::error::Error;

pub trait IndicatorError: Error {
    fn indicator_name(&self) -> Cow<'static, str>;
}

pub trait BackendError: Error {
    fn backend_name(&self) -> Cow<'static, str>;
}

pub trait DrawerError: Error {
    fn drawer_name(&self) -> Cow<'static, str>;
}

#[derive(Error, Debug)]
pub enum GenericError {
    #[error(display = "indicator error: {}", _0)]
    IndicatorError(Box<dyn IndicatorError>),

    #[error(display = "backend error: {}", _0)]
    BackendError(Box<dyn BackendError>),

    #[error(display = "drawer error: {}", _0)]
    DrawerError(Box<dyn DrawerError>),
}
