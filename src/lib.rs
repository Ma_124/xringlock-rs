use crate::backend::{Backend, BackendEvent};
use crate::error::GenericError;
use crate::indicator::{Indicator, RenderOptions, RenderReason};
use std::ops::DerefMut;
use std::time::Instant;

pub mod backend;
pub mod drawer;
pub mod error;
pub mod implementation;
pub mod indicator;
pub mod util; // TODO make private

pub fn prompt<I: Indicator, B: Backend, R: DerefMut<Target = [u8; CAP]>, const CAP: usize>(
    resp: &mut R,
    epoch: Instant,
    ind: &mut I,
    back: &mut B,
    opts: &mut RenderOptions,
) -> Result<usize, GenericError> {
    let mut pw_len = 0;

    loop {
        back.flush()?;
        let ev;
        if opts.frames_per_second == 0 {
            ev = Some(back.next_event()?);
        } else {
            ev = back.poll_event(1000 / opts.frames_per_second)?;
        }

        let rr;
        if let Some(ev) = ev {
            rr = match ev {
                BackendEvent::Char(c) => {
                    if pw_len + c.len_utf8() >= CAP {
                        pw_len = 0;
                        RenderReason::Clear
                    } else {
                        c.encode_utf8(&mut resp.deref_mut()[pw_len..]);
                        pw_len += c.len_utf8();
                        RenderReason::CharData(c)
                    }
                }
                BackendEvent::Backspace => {
                    pw_len = pw_len.saturating_sub(1);
                    RenderReason::Backspace
                }
                BackendEvent::Clear => {
                    pw_len = 0;
                    RenderReason::Clear
                }
                BackendEvent::Enter => return Ok(pw_len),
                _ => RenderReason::Frame,
            };
        } else {
            rr = RenderReason::Frame;
        }
        back.draw(epoch.elapsed(), ind, rr, opts)?;
    }
}
