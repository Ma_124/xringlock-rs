use std::convert::TryFrom;
use std::str::FromStr;

#[derive(Clone, Copy)]
pub struct Point(pub f64, pub f64);

pub trait Drawer {
    fn id(&self) -> u64;

    fn dimensions(&self) -> (f64, f64);

    fn use_color(&mut self, c: RGBA);
    fn use_stroke_width(&mut self, w: f64);

    fn arc(&mut self, center: Point, radius: f64, start_angle: f64, end_angle: f64);
    fn line(&mut self, a: Point, b: Point);

    fn fill_background(&mut self);
    fn fill(&mut self);
    fn stroke(&mut self);

    fn flush(&mut self);
}

#[derive(Debug, Copy, Clone)]
pub struct RGBA(pub f64, pub f64, pub f64, pub f64);

impl RGBA {
    pub fn with_alpha(&self, a: f64) -> Self {
        Self(self.0, self.1, self.2, self.3 * a)
    }
}

#[derive(Debug, Clone)]
pub enum Color {
    RGBA(RGBA),
    Random { a: f64 },
}

impl Color {
    pub fn none() -> Color {
        Color::RGBA(RGBA(0.0, 0.0, 0.0, 1.0))
    }

    pub fn random() -> Color {
        Color::Random { a: 1.0 }
    }

    pub fn rgba(&self) -> RGBA {
        match self {
            Color::RGBA(c) => *c,
            Color::Random { a } => RGBA {
                0: rand::random(),
                1: rand::random(),
                2: rand::random(),
                3: *a,
            },
        }
    }
}

impl From<RGBA> for Color {
    fn from(rgba: RGBA) -> Self {
        Self::RGBA(rgba)
    }
}

impl FromStr for Color {
    // TODO proper error
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "rand" => return Ok(Self::Random { a: 1.0 }),
            "none" => return Ok(Self::none()),
            _ => {}
        }
        if let Some(hex) = s.strip_prefix('#') {
            // TODO other sizes
            return Ok(match hex.len() {
                6 => Color::RGBA(RGBA(
                    u8::from_str_radix(&hex[0..2], 16).map_err(|_e| ())? as f64 / 255.0,
                    u8::from_str_radix(&hex[2..4], 16).map_err(|_e| ())? as f64 / 255.0,
                    u8::from_str_radix(&hex[4..6], 16).map_err(|_e| ())? as f64 / 255.0,
                    1.0,
                )),
                8 => Color::RGBA(RGBA(
                    u8::from_str_radix(&hex[0..2], 16).map_err(|_e| ())? as f64 / 255.0,
                    u8::from_str_radix(&hex[2..4], 16).map_err(|_e| ())? as f64 / 255.0,
                    u8::from_str_radix(&hex[4..6], 16).map_err(|_e| ())? as f64 / 255.0,
                    u8::from_str_radix(&hex[6..8], 16).map_err(|_e| ())? as f64 / 255.0,
                )),
                _ => return Err(()),
            });
        }
        Err(())
    }
}

impl TryFrom<&str> for Color {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Color::from_str(value)
    }
}
