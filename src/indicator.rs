use crate::drawer::Drawer;
use crate::error::GenericError;
use std::cell::RefCell;
use std::time::Duration;

#[non_exhaustive]
#[derive(Debug)]
pub enum RenderReason {
    Init,
    Frame,
    CharHash(u64),
    CharData(char),
    Backspace,
    Clear,
    AuthStart,
    AuthSuccess,
    AuthFailure,
    CapslockChanged(bool),
}

#[derive(Debug)]
pub struct RenderOptions {
    pub frames_per_second: u16,
}

impl Default for RenderOptions {
    fn default() -> Self {
        Self {
            frames_per_second: 0,
        }
    }
}

pub trait Indicator {
    fn render(
        &mut self,
        time: Duration,
        drawers: &mut [RefCell<Box<dyn Drawer>>],
        reason: RenderReason,
        opts: &mut RenderOptions,
    ) -> Result<(), GenericError>;
}
