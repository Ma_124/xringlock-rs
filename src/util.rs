use std::io;
use std::io::{ErrorKind, Read};

macro_rules! read_char_impl {
    ($r: expr, $len: expr, $first: expr) => {
        let mut buf = [0; $len];
        Read::read_exact($r, &mut buf[1..])?;
        buf[0] = $first;
        return Ok(std::str::from_utf8(&buf)
            .map_err(|_| ErrorKind::InvalidData)?
            .chars()
            .next()
            .unwrap());
    };
}

pub trait ReadCharExt: Read {
    fn read_byte(&mut self) -> Result<u8, io::Error> {
        let mut first = [0; 1];
        self.read_exact(&mut first)?;
        Ok(first[0])
    }

    fn read_char(&mut self) -> Result<char, io::Error> {
        let first = self.read_byte()?;
        match first.leading_ones() {
            0 => Ok(first as char),
            2 => {
                read_char_impl!(self, 2, first);
            }
            3 => {
                read_char_impl!(self, 3, first);
            }
            4 => {
                read_char_impl!(self, 4, first);
            }
            _ => Err(ErrorKind::InvalidData.into()),
        }
    }
}

impl<R: Read> ReadCharExt for R {}
